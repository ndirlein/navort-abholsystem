package com.navort.msg.navort;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
  private Button btnDriver, btnCustomer;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    btnDriver = findViewById(R.id.driver);
    btnCustomer = findViewById(R.id.customer);

    btnDriver.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent(MainActivity.this, DriverLoginActivity.class);
        startActivity(intent);
        finish();
        return;
      }
    });

    btnCustomer.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent(MainActivity.this, CustomerLoginActivity.class);
        startActivity(intent);
        finish();
        return;
      }
    });
  }
}
