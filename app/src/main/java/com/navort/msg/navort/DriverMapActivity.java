package com.navort.msg.navort;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Debug;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Button;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

public class DriverMapActivity extends FragmentActivity
        implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        //com.google.android.gms.location.LocationListener,
        RoutingListener{



  BluetoothAdapter mBluetoothAdapter;
  BluetoothSocket mmSocket;
  BluetoothDevice mmDevice;
  OutputStream mmOutputStream;
  InputStream mmInputStream;
  Thread workerThread;
  byte[] readBuffer;
  int readBufferPosition;
  volatile boolean stopWorker;
  boolean nextLine = false;

  private GoogleMap mMap;
  GoogleApiClient googleApiClient;
  Location lastLocation;

  // is used for built in gps sensor
  //LocationRequest locationRequest;

  private String customerId = "";

  private Button btnLogout;
  private boolean isLoggingOut = false;

  private SupportMapFragment mapFragment;

  private Marker currentLocationMarker;
  private Marker pickupMarker;
  private DatabaseReference customerPickupLocRef;
  private ValueEventListener customerPickupLocRefListener;
  final int LOCATION_REQUEST_CODE = 1;

  private List<Polyline> polylines;
  private static final int[] COLORS = new int[]{R.color.primary_dark_material_light};


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_driver_map);

    findBT();

    try
    {
      openBT();
    }
    catch (IOException ex) { }


    // Obtain the SupportMapFragment and get notified when the map is ready to be used.
    mapFragment = (SupportMapFragment) getSupportFragmentManager()
            .findFragmentById(R.id.map);

    polylines = new ArrayList<>();

    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
    {
      ActivityCompat.requestPermissions(DriverMapActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
    }
    mapFragment.getMapAsync(this);

    btnLogout = findViewById(R.id.logout);
    btnLogout.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        isLoggingOut = true;

        disconnectDriver();

        FirebaseAuth.getInstance().signOut();
        Intent intent = new Intent(DriverMapActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
        return;
      }
    });

    getAssignedCustomer();
  }

  /**
   * finds the bluetooth module and saves it to the variable mmDevice
   */
  void findBT()
  {
    mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    if(mBluetoothAdapter == null)
    {

    }

    if(!mBluetoothAdapter.isEnabled())
    {
      Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
      startActivityForResult(enableBluetooth, 0);
    }

    Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
    if(pairedDevices.size() > 0)
    {
      for(BluetoothDevice device : pairedDevices)
      {
        if(device.getName().equals("RNBT-6DB3"))
        {
          mmDevice = device;
          break;
        }
      }
    }
  }

  /**
   * connects to the bluetooth module and reads/writes inputs/outputs from the UART serial port
   * @throws IOException
   */
  void openBT() throws IOException
  {
    UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"); //Standard SerialPortService ID
    mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
    mmSocket.connect();
    mmOutputStream = mmSocket.getOutputStream();
    mmInputStream = mmSocket.getInputStream();

    //beginListenForData();

    //myLabel.setText("Bluetooth Opened");
  }

  /**
   * Reads the UART input and makes it readable by converting it to ASCII characters
   */
  void beginListenForData()
  {
    final Handler handler = new Handler();
    final byte delimiter = 10; //This is the ASCII code for a newline character

    stopWorker = false;
    readBufferPosition = 0;
    readBuffer = new byte[1024];
    workerThread = new Thread(new Runnable()
    {
      public void run()
      {
        int test = 0;
        while(!Thread.currentThread().isInterrupted() && !stopWorker)
        {
          try
          {
            int bytesAvailable = mmInputStream.available();
            if(bytesAvailable > 0)
            {
              byte[] packetBytes = new byte[bytesAvailable];
              mmInputStream.read(packetBytes);
              for(int i=0;i<bytesAvailable;i++)
              {

                test++;
                byte b = packetBytes[i];
                if(b == delimiter || test == 100)
                {
                  nextLine = false;
                  test = 0;
                  byte[] encodedBytes = new byte[readBufferPosition];
                  System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
                  final String data = new String(encodedBytes, "US-ASCII");
                  readBufferPosition = 0;

                  handler.post(new Runnable()
                  {
                    public void run()
                    {

                      //writer.println(data + "");

                      if(data.contains("GNGLL")){
                        System.out.println(data);
                        if(data.length() > 33) {
                          System.out.println("Cropped data: " + data.substring(7, 17) + "  " + data.substring(20, 31));

                          double degreesla = Double.parseDouble(data.substring(7, 9));
                          double minutesla = Double.parseDouble(data.substring(9, 17));

                          double degreeslo = Double.parseDouble(data.substring(20, 23));
                          double minuteslo = Double.parseDouble(data.substring(23, 27));

                          double latitude = degreesla + (minutesla / 60);
                          double longtidude = degreeslo + (minuteslo / 60);

                          changeLocationM8U(new LatLng(latitude, longtidude));
                        }else{
                          System.out.println("no signal");

                        }
                      }

                      //Debug.log("txt");
                      //System.out.println(data);
                      //myLabel.setText(data);
                    }
                  });
                }
                else
                {
                  readBuffer[readBufferPosition++] = b;
                }
              }
            }
          }
          catch (IOException ex)
          {
            stopWorker = true;
          }
        }
      }
    });

    workerThread.start();
  }

  /**
   * Gets the assigned customer, if there is one, and saves their customerId to a variable
   */
  private void getAssignedCustomer() {
    String driverId = FirebaseAuth.getInstance().getCurrentUser().getUid();
    DatabaseReference customerRef = FirebaseDatabase.getInstance().getReference()
            .child("Users").child("Drivers").child(driverId).child("customerRideId");
    customerRef.addValueEventListener(new ValueEventListener() {
      @Override
      public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        if (dataSnapshot.exists()) {
          customerId = dataSnapshot.getValue().toString();
          getAssignedCustomerPickupLocation();
        }
        else {
          cancelRide();
        }
      }

      @Override
      public void onCancelled(@NonNull DatabaseError databaseError) {

      }
    });
  }

  /**
   * Erases everything from that's related to the customer if the customer cancels the ride
   */
  private void cancelRide() {
    erasePolylines();

    String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
    DatabaseReference driverRef = FirebaseDatabase.getInstance().getReference().child("Users").child("Drivers").child(userId).child("customerRideId");
    driverRef.removeValue();

    DatabaseReference requestRef = FirebaseDatabase.getInstance().getReference("request");
    GeoFire geoFire = new GeoFire(requestRef);
    geoFire.removeLocation(customerId, new GeoFire.CompletionListener() {
      @Override
      public void onComplete(String key, DatabaseError error) {

      }
    });

    customerId = "";
    if (pickupMarker != null) {
      pickupMarker.remove();
    }
    if (customerPickupLocRefListener != null) {
      customerPickupLocRef.removeEventListener(customerPickupLocRefListener);
    }
  }

  /**
   * Get the location of the customer pickup request and display it on the map
   */
  private void getAssignedCustomerPickupLocation() {
    customerPickupLocRef = FirebaseDatabase.getInstance().getReference().child("request").child(customerId).child("l");
    customerPickupLocRefListener = customerPickupLocRef.addValueEventListener(new ValueEventListener() {
      @Override
      public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        if (dataSnapshot.exists() && !customerId.equals("")) {
          List<Object> map = (List<Object>) dataSnapshot.getValue();
          double locationLat = 0;
          double locationLng = 0;
          if(map.get(0) != null) {
            locationLat = Double.parseDouble(map.get(0).toString());
          }
          if(map.get(1) != null) {
            locationLng = Double.parseDouble(map.get(1).toString());
          }
          LatLng pickupLatLng = new LatLng(locationLat, locationLng);

          pickupMarker = mMap.addMarker(new MarkerOptions().position(pickupLatLng).title("Abhol-Position")
                  .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_pickup)));

          getRouteToMarker(pickupLatLng);
        }
      }

      @Override
      public void onCancelled(@NonNull DatabaseError databaseError) {

      }
    });
  }

  /**
   * Draws a route on the map with polylines
   * @param pickupLatLng Location of the pickup request
   */
  private void getRouteToMarker(LatLng pickupLatLng) {
    Routing routing = new Routing.Builder()
            .travelMode(AbstractRouting.TravelMode.DRIVING)
            .withListener(this)
            .alternativeRoutes(false)
            .waypoints(new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude()), pickupLatLng)
            .build();
    routing.execute();
  }


  /**
   * Manipulates the map once available.
   * This callback is triggered when the map is ready to be used.
   * This is where we can add markers or lines, add listeners or move the camera. In this case,
   * we just add a marker near Sydney, Australia.
   * If Google Play services is not installed on the device, the user will be prompted to install
   * it inside the SupportMapFragment. This method will only be triggered once the user has
   * installed Google Play services and returned to the app.
   */
  @Override
  public void onMapReady(GoogleMap googleMap) {
    mMap = googleMap;

    // Add a marker in Sydney and move the camera
    /*LatLng sydney = new LatLng(-34, 151);
    mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
    mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));*/

    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
    {
      return;
    }
    lastLocation = new Location("LastLocation");
    buildGoogleApiClient();


    //mMap.setMyLocationEnabled(true);
  }

  protected synchronized void buildGoogleApiClient() {
    googleApiClient = new GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build();

    googleApiClient.connect();
  }

  /**
   * Changes the location according to the parameter and shows it on the map
   * @param location the latitude and longitude in decimal degrees
   */
  private void changeLocationM8U(LatLng location) {
    if (getApplicationContext() != null) {
      lastLocation.setLatitude(location.latitude);
      lastLocation.setLongitude(location.longitude);

      mMap.moveCamera(CameraUpdateFactory.newLatLng(location));
      mMap.animateCamera(CameraUpdateFactory.zoomTo(15));

      if (currentLocationMarker != null) {
        currentLocationMarker.setPosition(location);
      }
      else {
        currentLocationMarker =  mMap.addMarker(new MarkerOptions().position(location).title("Current position")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_car)));
      }

      String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
      DatabaseReference dbRefDriversAvailable = FirebaseDatabase.getInstance().getReference("driversAvailable");
      DatabaseReference dbRefDriversWorking = FirebaseDatabase.getInstance().getReference("driversWorking");

      GeoFire geoFireAvailable = new GeoFire(dbRefDriversAvailable);
      GeoFire geoFireWorking = new GeoFire(dbRefDriversWorking);

      switch (customerId) {
        case "":
          geoFireWorking.removeLocation(userId, new GeoFire.CompletionListener() {
            @Override
            public void onComplete(String key, DatabaseError error) {

            }
          });
          geoFireAvailable.setLocation(userId, new GeoLocation(lastLocation.getLatitude(), lastLocation.getLongitude()), new GeoFire.CompletionListener() {
            @Override
            public void onComplete(String key, DatabaseError error) {

            }
          });
          break;
        default:
          geoFireAvailable.removeLocation(userId, new GeoFire.CompletionListener() {
            @Override
            public void onComplete(String key, DatabaseError error) {

            }
          });
          geoFireWorking.setLocation(userId, new GeoLocation(lastLocation.getLatitude(), lastLocation.getLongitude()), new GeoFire.CompletionListener() {
            @Override
            public void onComplete(String key, DatabaseError error) {

            }
          });
          break;
      }
    }
  }

/*  @Override
  public void onLocationChanged(Location location) {
    if (getApplicationContext() != null) {
      lastLocation = location;

      LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

      mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
      mMap.animateCamera(CameraUpdateFactory.zoomTo(15));

      String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
      DatabaseReference dbRefDriversAvailable = FirebaseDatabase.getInstance().getReference("driversAvailable");
      DatabaseReference dbRefDriversWorking = FirebaseDatabase.getInstance().getReference("driversWorking");

      GeoFire geoFireAvailable = new GeoFire(dbRefDriversAvailable);
      GeoFire geoFireWorking = new GeoFire(dbRefDriversWorking);

      switch (customerId) {
        case "":
          geoFireWorking.removeLocation(userId, new GeoFire.CompletionListener() {
            @Override
            public void onComplete(String key, DatabaseError error) {

            }
          });
          geoFireAvailable.setLocation(userId, new GeoLocation(location.getLatitude(), location.getLongitude()), new GeoFire.CompletionListener() {
            @Override
            public void onComplete(String key, DatabaseError error) {

            }
          });
          break;
        default:
          geoFireAvailable.removeLocation(userId, new GeoFire.CompletionListener() {
            @Override
            public void onComplete(String key, DatabaseError error) {

            }
          });
          geoFireWorking.setLocation(userId, new GeoLocation(location.getLatitude(), location.getLongitude()), new GeoFire.CompletionListener() {
            @Override
            public void onComplete(String key, DatabaseError error) {

            }
          });
          break;
      }
    }
  }*/

  @Override
  public void onConnected(@Nullable Bundle bundle) {

    /*if (ActivityCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH) != PackageManager.PERMISSION_GRANTED)
    {
      ActivityCompat.requestPermissions(DriverMapActivity.this, new String[]{Manifest.permission.BLUETOOTH}, LOCATION_REQUEST_CODE);
    }*/

    beginListenForData();

    //LatLng sydney = new LatLng(49.1437, 10.0449);

    //changeLocationM8U(sydney);

    //implementation with built in gps sensor
    /*locationRequest = new LocationRequest();
    locationRequest.setInterval(1000);
    locationRequest.setFastestInterval(1000);
    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
    {
      ActivityCompat.requestPermissions(DriverMapActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
    }

    if (googleApiClient.isConnected()) {
      LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }*/
  }

  @Override
  public void onConnectionSuspended(int i) {

  }

  @Override
  public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

  }

  /**
   * Deletes the driver from the database when he logs out
   */
  private void disconnectDriver() {
    if (googleApiClient != null) {
      //LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);

      workerThread.interrupt();

      String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
      DatabaseReference dbReference = FirebaseDatabase.getInstance().getReference("driversAvailable");

      GeoFire geoFire = new GeoFire(dbReference);
      geoFire.removeLocation(userId, new GeoFire.CompletionListener() {
        @Override
        public void onComplete(String key, DatabaseError error) {

        }
      });
    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    switch (requestCode) {
      case LOCATION_REQUEST_CODE: {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          mapFragment.getMapAsync(this);
        } else {
          Toast.makeText(getApplicationContext(), "Please provide the permission", Toast.LENGTH_LONG).show();
        }
        break;
      }
    }
  }

  @Override
  protected void onStop() {
    super.onStop();

    if (!isLoggingOut) {
      disconnectDriver();
    }
  }

  @Override
  public void onRoutingFailure(RouteException e) {
    if(e != null) {
      Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
    }else {
      Toast.makeText(this, "Something went wrong, Try again", Toast.LENGTH_SHORT).show();
    }
  }

  @Override
  public void onRoutingStart() {

  }

  @Override
  public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex) {
    if(polylines.size()>0) {
      for (Polyline poly : polylines) {
        poly.remove();
      }
    }

    polylines = new ArrayList<>();
    //add route(s) to the map.
    for (int i = 0; i <route.size(); i++) {

      //In case of more than 5 alternative routes
      int colorIndex = i % COLORS.length;

      PolylineOptions polyOptions = new PolylineOptions();
      polyOptions.color(getResources().getColor(COLORS[colorIndex]));
      polyOptions.width(10 + i * 3);
      polyOptions.addAll(route.get(i).getPoints());
      Polyline polyline = mMap.addPolyline(polyOptions);
      polylines.add(polyline);

      Toast.makeText(getApplicationContext(),"Route "+ (i+1) +": distance - "+ route.get(i).getDistanceValue()+": duration - "+ route.get(i).getDurationValue(),Toast.LENGTH_SHORT).show();
    }
  }

  @Override
  public void onRoutingCancelled() {

  }

  /**
   * Deletes the polylines from the map
   */
  private void erasePolylines() {
    for (Polyline line : polylines) {
      line.remove();
    }
    polylines.clear();
  }
}
