package com.navort.msg.navort;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class CustomerLoginActivity extends AppCompatActivity {
  private EditText etEmail, etPassword;
  private Button btnLogin, btnRegistration;

  private FirebaseAuth fbAuth;
  private FirebaseAuth.AuthStateListener firebaseAuthListener;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_customer_login);

    fbAuth = FirebaseAuth.getInstance();

    firebaseAuthListener = new FirebaseAuth.AuthStateListener() {
      @Override
      public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user != null) {
          Intent intent = new Intent(CustomerLoginActivity.this, CustomerMapActivity.class);
          startActivity(intent);
          finish();
          return;
        }
      }
    };

    etEmail = findViewById(R.id.email);
    etPassword = findViewById(R.id.password);

    btnLogin = findViewById(R.id.login);
    btnRegistration = findViewById(R.id.registration);

    btnRegistration.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        final String email = etEmail.getText().toString();
        final String password = etPassword.getText().toString();
        fbAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(CustomerLoginActivity.this, new OnCompleteListener<AuthResult>() {
          @Override
          public void onComplete(@NonNull Task<AuthResult> task) {
            if(!task.isSuccessful()) {
              Toast.makeText(CustomerLoginActivity.this, "Registrierung fehlgeschlagen", Toast.LENGTH_SHORT).show();
            }
            else {
              String user_id = fbAuth.getCurrentUser().getUid();
              DatabaseReference current_user_db = FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(user_id);
              current_user_db.setValue(true);
            }
          }
        });
      }
    });

    btnLogin.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        final String email = etEmail.getText().toString();
        final String password = etPassword.getText().toString();
        fbAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(CustomerLoginActivity.this, new OnCompleteListener<AuthResult>() {
          @Override
          public void onComplete(@NonNull Task<AuthResult> task) {
            if(!task.isSuccessful()) {
              Toast.makeText(CustomerLoginActivity.this, "Login fehlgeschlagen", Toast.LENGTH_SHORT).show();
            }
          }
        });
      }
    });
  }

  @Override
  protected void onStart() {
    super.onStart();
    fbAuth.addAuthStateListener(firebaseAuthListener);
  }

  @Override
  protected void onStop() {
    super.onStop();
    fbAuth.removeAuthStateListener(firebaseAuthListener);
  }
}
